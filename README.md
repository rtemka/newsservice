### **Сервис агрегации новостей**

Является частью децентрализованного [**приложения**](https://github.com/rtemka/agg)

Сервис собирает rss-новости по заданному интервалу времени и отдает их через REST API.

#### API эндпоинты

```bash
# запрос c пагинацией
curl -X GET http://[host:port]/news

# Допустимые параметры запроса:

# page=NUM                          # номер страницы
# s=STRING                          # поиск по заголовку новости
# exc=STRING                        # исключить заголовки содержащие этот текст
# date=[gte:lte:lt:gt]YYYY-MM-DD    # поиск по конктретной дате новости
# dateEnd=[lt:lte]YYYY-MM-DD        # ограничить диапазон даты сверху
# sortBy={title | date | match}     # сортировать итоговый результат

# ответ 200 OK..
# {
#   "total_pages": 999,
#   "page_size": 10,
#   "page_number": 1,
#   "page": [
#         {
#             "id": 1,
#            "title": "test title",
#            "pubTime": 1661338087,
#            "content": "test content",
#            "link": "https://example.com"
#         }, ...
```

```bash
# запрос по id
curl -X GET http://[host:port]/news/1

# ответ 200 OK..
#         {
#             "id": 1,
#            "title": "test title",
#            "pubTime": 1661338087,
#            "content": "test content",
#            "link": "https://example.com"
#         }
```